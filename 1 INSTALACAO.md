# Instalação do Git

[https://git-scm.com/downloads](https://git-scm.com/downloads)

## Windows

só fazer o download e instalar com next next next.

[https://git-scm.com/download/win](https://git-scm.com/download/win)

## Linux

Em distro basedas em debian:

```bash
sudo apt-get install git
```

Para outras, confira a documentação, mas resumidamente é só ir no instalador de pacote e mandar executar a instalação do pacote git.

[https://git-scm.com/download/linux](https://git-scm.com/download/linux)

## Mac

Fazer o download do pkg e instalar. Em alguns casos é necessário liberar no sistema de segurança do mac a instalação de fontes desconhecidas.

[https://git-scm.com/download/mac](https://git-scm.com/download/mac)

Recomendo fortemente que saiba usar o **Git no terminal** e evite uso do Git Desktop ou qualquer outro sistema de interface gráfica. Só desta maneira aprenderá de fato a ferramenta.

Depois que tiver bem dominado, exitem extensões no vscode que facilitam bem a vida.

Para ter certeza que o git foi instalado faça o comando no terminal

```bash
git --version
```

Recomendo primeiro ler o documento até o final para depois iniciar os comandos de teste e prática. Entenda o conceito e depois aplique.

## Architetura

![git](./pics/gitarch.png)
