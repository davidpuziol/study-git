# Versionamento com Data

Com a quantidade de entrega sendo muito alta pelas evoluções do CI (Continuos Integration Continuos Deploy), se encontrar no versionamento semântico pode se tornar mais custoso.

A proposta abaixo tem sido usado no Home Assistant e tem dado muito certo na comunidade.

Acredito que toda a empresa poderia entender o versionamento de forma muito rápida e eficaz?

Observe:

Quando se tem somente 1 deploy por dia, pode ser utilizada a seguinte proposta.

**2021.5.2**

ano.mes.versão

Quando acontece mais de um deploy por dia, a proposta abaixo se torna mais interessante.

**2021.5.3.2**

ano.mes.dia.versão
