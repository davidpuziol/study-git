
# Comandos

Aqui abaixo alguns comandos do git mais usados

## Clone

Útil para clonar um repositório em algum servidor Github, Gitlab ou outros.

```bash
git clone urldorepositorio.git
```

É possível também clonar um repositório local passando a pasta ao invés da url

```bash
# clonar o projeto x para o projeto y
git clone pasta/x pasta2/y
```

para mais informações

[https://git-scm.com/docs/git-clone](https://git-scm.com/docs/git-clone)

## Comando Add

Para rastrear os arquivos e passar eles para stage é utilizados os possíveis comandos abaixo.

```bash
git add nomedoarquivo
# para mais de um arquivo
git add nomearquivo1 nomearquivo2
```

Adicionar todos os arquivos

```bash
git add -A
git add --all
```

Adicio somente os arquivos do diretório corrente

```bash
git add .
```

O comando inverso do add é o rm que serve para remover os arquivos. Com o git rm você pode retirar a rastreabilidade de um determinado arquivo ou até mesmo voltar um arquivo do estado de staged para unstaged.

```bash
# vai remover desse diretório
git rm ./dir
# -r é recursovio para percorrer todos os diretório abaixo desse
git rm -r ./dir
# somente uma lista de arquivos
git rm arq1 arq2
# vai remover de staged e passar para unstaged
git rm --cached arq1
```

### .gitignore

Se você criar um arquivo .gitignore na raíz do seu projeto, todos os arquivos ou diretórios ali listados serão ignorados durante o comando add. Permanecerão sempre fora da rastreabilidade do git.

Exemplo de .gitignore:

```bash
*.bmp     (exclui todos os arquivos dessa extensão)

bla/*.bmp     (exclui todos os arquivos dessa extensão na pasta bla)
```

## Comando LS

Comando para listar arquivos que estão sendo rastreados ou seja, que já foram adicionados

```bash
git ls-files -s
git ls --stage
```

Mostra somente os arquivos modificados

```bash
git ls-files -m 
git ls-files --modified
```

Mostra a lista dos arquivos não rastreados

```bash
git ls-files -o ou 
git ls-files --others
```

para mais informações

[https://git-scm.com/docs/git-ls-files](https://git-scm.com/docs/git-ls-files)

## Status

Server para conferir o status do git e dos arquivos, se existe algo que mudou.

```bash
git status
```

para uma saída menor e a branch que está.

```bash
git status -bs
```

para mais informações

[https://git-scm.com/docs/git-status](https://git-scm.com/docs/git-status)

## Comando Commit

Para fazer um commit, ou seja criar um ponto de versionando para o arquivo. Um comite consolida um ponto de restauração no repositório local. Todo commit é acompanhado de uma mensagem.

**Como uma boa prática, sempre faça mensagens para que encontre mais facilmente na hora de fazer um rollback**

```bash
# adicionad todos os arquivos
git commit -m “mensagem” todos os arquivos

# também adicionado todos os arquivos
git commit -m “mensagem” * todos os arquivos

# commit somente com um arquivo especificado
git commit -m “mensagem” arquivo1 arquivo2 arquivo3
```

Em alguns casos você precisa mudar algo quando tudo já foi comitado e para não gerar mais um commit você precisa acrescentar algo. Para não gerar muitos, utilize o comando amend, pois este vai acrescentar as mudanças no mesmo commit.

```bash
git commit --amend  
```

Para mais informações

[https://git-scm.com/docs/git-commit](https://git-scm.com/docs/git-commit)

Entendendo novamente o sistema de estados de um arquivo antes dele ir para um repositório em algum servidor.

Um commit sobe o que está na **Stage para o Head**, porém localmente.

Um atalho rápido para adicionar todos os arquivos e commitar ao mesmo tempo, mas só funcionar com arquivos que já são rastreados pelo git.

```bash
git commit -a -m “mensagem”
```

NÃO É POSSÍVEL DESFAZER UM COMMIT NO REPOSITÓRIO REMOTO, TEM QUE FAZER UM COMMIT POR CIMA DE CORREÇÃO.

## Comando Diff

Com esse comando podemos verificar as alterações realizadas no arquivos do diretório de trabalho (copia local) em comparação com os arquivos que já foram consolidados(comitados). Lembrando que é necessário que o arquivo tenha sido comitado pelo menos uma vez.

Para arquivos aque ainda não passaram para área de staged

```bash
# Para todos os arquivos
git diff 
# Para especificar quais arquivos quer ver
git diff arq1
git diff arq1 arq2
```

Se um arquivo já foi adicionado ao staged área com o comando add, por exemplo é necessário utilizar

```bash
# mostra a diferença do que tem no último commit com o que tem na área de staged
git diff --staged
# Para especificar um arquivo ou mais
git diff --staged arq1
```

Caso queira ver a diferença somente de alguns arquivos

```bash
git diff --staged arq1 arq2
# para arquivos da working área
git diff arq1
```

Para ver os estados em cached use --cached

 Por padrão o git funciona comparando com o ultimo commit no caso o HEAD

 **git diff HEAD é a mesma coisa que git diff**, logo é possível passar exatamente o commit com o qual você quer comparar o seu arquivo se substituir o HEAD pelo hash do commit.

Para entender um pouco melhor até agora como isso funciona

![fluxogit](./pics/fluxogit.jpg)

Para mais informações

[https://git-scm.com/docs/git-diff](https://git-scm.com/docs/git-diff)

## Comando Log

Para ver o histórico de commits, usamos o log. Quando o seu projeto está atualizando com o do repositório ele trás o log de todos em todas as branches.

```bash
# Para todos os commits
git log
# Para os 3 ultimos commits
git log -3
# Para mostrar resumido
git log --oneline
# Para filtrar por autor
git log --author=”Fulano Tal” 
# Para mostrar as branches de forma grafica com seus respectivos commits
git log --graph --decorate
# Para mostrar somente os commits de uma branch
git log origin/nomedabranch --oneline  mostra os commits da branch
```

O hash do commit é o que você precisa para reverter um commit. No comando log ele mostra onde está o seu Head e os hashes dos comites.

Para mais informações

[https://git-scm.com/docs/git-log](https://git-scm.com/docs/git-log)

## Comando Checkout

Usado para ir para um ponto específico de um projeto, seja commit, branches e tags.

Para voltar em algum ponto de commit é usado checkout e o hash do commit.

O checkout também é usado para mudar de branch como veremos posteriormente.

```bash
# Só os 5 ou 6 primeiros caracteres do hash é suficiente
git checkout meuhash
# Voltando para o ultimo commit de uma branch
git checkout nomedabranch
```

Se você estiver mudando de branch e os arquivos daquela branch não estiverem commitados, em tese eles seriam perdidos e o git não deixa você fazer isso. Você precisa comitar para salvar ou dá um reset --hard como mostrado abaixo.

Ainda sim é possível usar o stash para salvar o que você fez em uma pilha como será visto mais pra frente

```bash
## Para ir para a versão do arquivo que esteja no ultimo commit da branch que você está.
git checkout nomedoarquivo

## o comando switch também muda de branch, dá no mesmo
## git switch nomedabranch
git checkout nomedabranch

# Cria uma branch já passando para ela
git checkout -b nomedabranch
```

Para mais informações

[https://git-scm.com/docs/git-checkout](https://git-scm.com/docs/git-checkout)

## Reset x Clean

O comando reset serve para redefinir a HEAD para um estado especificado.

git reset --hard é como se você desse um checkout em todos os arquivos e de fato volta o estado inicial daquele ponto de partida, ou commit

git reset --hard HEAD~n desfazer os commits sendo n o número de commits que você quer desfazer

Um detalhe interessante é que se você criar um arquivo na working dir e ele nunca tiver ido para a área de Staged, ou seja, o git nunca rastreou este arquivo, mesmo que você volte commit ou até de um checkout para o Head daquela branch ele continuará no projeto.

Para deletar de fato um arquivo não rastreado faça

```bash
git clean -f
```

Para mais informações

[https://git-scm.com/docs/git-reset](https://git-scm.com/docs/git-reset)

[https://git-scm.com/docs/git-clean](https://git-scm.com/docs/git-clean)

## Comando Branch

Usado para manipular as ramificações localmente.

Entendendo como funciona uma branch

A branch é uma ramificação para que o projeto siga por N caminhos a partir de um ponto. Se você tem uma versão estável, ou seja, a branch main, você pode desenvolver várias versões do projeto sem impactar na sua versão estável. A branch é uma ramificação a nível de projeto, não confundir com um fork.

Uma boa prática é deixar de chamar a branch principal de master e chamar de main.

```bash
#  lista as branches locais
git branch
# cria uma nova branch localmente
git branch novabranch
# deleta uma branch localmente. Voce não pode estar na mesma branch que deseja deletar
git branch -d nomedabranch 
# para forçar a remoção da branch
git branch -D nomedabranch
# renomear a branch que você esta
git branch -m novonome
# renomeia outra branch
git branch -m nomedabranch novonome
```

Para mais informações

[https://git-scm.com/docs/git-branch/pt_BR](https://git-scm.com/docs/git-branch/pt_BR)

## Comando Push

Usado para dar comando direto no repositório remoto a qual o seu projeto esta vinculado (origin). Tudo que vai para o repositório leva essa palavra. O push sobe os arquivos locais da branch que você está para a respectiva branch no servidor Github, Gitblab, Bitbucket, etc.

É possível que você tenha mais de um origin no projeto. Exemplo seria ter o mesmo repositório em 2 servidores diferentes. Mas isso não é comum.

git push (vai tentar subir a branch para a principal, master/main caso você tenha definido como foi feito na amostra do comando config. Nesse caso, se você estiver em uma branch diferente vai dar erro. Não é uma boa prática fazer esse comando sem apontar a branch que você deseja para evitar publicar na master/main)

```bash
#Se não existir a branch ele vai criar e subir a branch para o origin.

git push origin minhabranch 

# se precisar passar algum origin diferentes
git push -u origin minhabranch
# mesma coisa ado comando acima
git push --set-upstream origin minhabranch
# Vai deletar do servidor remoto apontado em origin a branch especificada
git push --delete origin nomedabranch
```

Se for apagar uma branch no repositório deve ser alinhado com toda a equipe antes para evitar que outras pessoas que estão usando essa branch não a encontre, gerando problemas.

Curiosidade: o git não faz o push de um diretório vazio sem nenhum arquivo.

Para mais informações

[https://git-scm.com/docs/git-push](https://git-scm.com/docs/git-push)

## Comando Fetch

É quando você solicita para o servidor que traga as modificações do repositório para a máquina local, mas não aplica.

```bash
git fetch
```

Para mais informações

[https://git-scm.com/docs/git-fetch](https://git-scm.com/docs/git-fetch)

## Comando Pull

O comando pull é a soma do comando fetch com o comando merge. Ele puxa as alterações do servidor já faz o merge localmente.

git pull atualiza todo o conteúdo do servidor com o da sua máquina, para a branch que você esta

Se você der um git pull e existir algum branch diferente das suas locais, ela não aparece no comando git branch na listagem apesar de existir. Ela só aparece se você der um checkout para ela manualmente.

Se você criou uma branch localmente e ela não existe no servidor e você tentar dar um git pull, acontecerá um erro até que você crie a mesma branch igual no servidor.

usado para observar como está o projeto a frente sem sair do ponto que você está.

É ideal sempre passar o origin

```bash
git pull origin nomedabranch
```

um parâmetro que deve ser comentado é o --rebase
Esse parametro é um merge do que tem naquela branch do repositório com a sua branch local

```bash
git pull origin nomedabranch --rebase
```

Para mais informações

[https://git-scm.com/docs/git-pull](https://git-scm.com/docs/git-pull)

## Comando Merge

O merge é quando você mescla/junta as alterações de uma branch com outra.

Vá para a branch que receberá as alterações e o comando de merge puxando daquela que você deseja mergear.

Mas antes, tenha certeza que a branch que você esta, de fato esteja no ultimo commit, pois é possível que quando você passe para esta ramificação alguém já fez alteração nela. Para isso execute.

```bash
git checkout branch_que_sera_mesclada
git pull origin branch_que_sera_mesclada
```

git merge vai fazer o merge com a branch de mesmo nome que você, caso exista

Se você esta na main e quer mergear com a develop

```bash
git merge develop
```

git merge develop Se você estiver na branch main por exemplo ele irá trazer as alterações da develop para a main

O git merge faz as alterações e já faz um commit caso não tenha conflitos, pois ele cria um ponto de restauração.

Para mais informações

[https://git-scm.com/docs/git-merge](https://git-scm.com/docs/git-merge)

## Comando Remote

Quando você abre um projeto local e dá um git init nesse projeto para o git começar a versionar isso localmente, você não conseguirá dar um git push se ele não souber qual é o endereço do repositório com o qual ele está vinculado. O remote serve para vincular um projeto local a um repositório em um servidor remoto.

```bash
# adiciona o origin para o url
git remote add origin urlDoRepositorio.git
#   mostra a url do push e do fetch
git remote -v
# traz as mudanças do repositório, praticamente um fetch
git remote update
```

Para mais informações

[https://git-scm.com/docs/git-remote](https://git-scm.com/docs/git-remote)

## Comando Tags

Imagine uma tag como sendo um snapshot do projeto naquele momento. Diferente das branches eles são imutáveis. Se você der um checkout em uma tag e fazer commits nada mudará a não ser que crie uma branch primeiro a partir daquele ponto e inicie uma nova ramificação. Nos repositórios elas são as releases do projeto.

```bash
# lista as tags
git tag
# cria uma tag e a mensagem a partir de onde você esta
git tag -a nomedatag -m “mensagem para explicar essa tag” 
# cria uma tag a partir do commit
git tag -a nomedatag hashDoComite -m “mensagem de explicação” 
# envia a tag pro repositorio remoto
git push origin nomedatag
# deleta a tag localmente
git tag -d nomedatag 
# deleta a tag do repositorio remoto
git push --delete origin nomedatag
```

Para mais informações

[https://git-scm.com/docs/git-tag](https://git-scm.com/docs/git-tag)

## Comando Stash

Se você fez alguma modificação, mas ainda não está pronto para commitar pois ainda existe algum tipo de erro ou ainda não finalizou o que você está fazendo, ou até mesmo precisa trocar de branch para ajudar um amigo, para mudar de branch ou é preciso commitar ou reverter. Se comitar com erro não é boa prática, se reverter vai perder todo o trabalho. o Stash salva em memória o que você está fazendo para deixar para que possa mudar de branch. Funciona como uma pilha.

```bash
# salva a mudança
git stash save “Mensagem de lembrete”
# salva sem mensagem
git stash
# mostra a pilha de stashes
git stash list 
# traz o topo da pilha mas não deleta
git stash apply traz o topo da pilha
# vai para o stash especificado mas nao deleta
git stash apply stash@{n}
```

Dentro do comando stash temos o pop que faz a mesma coisa que o apply, mas já deleta

```bash
# vai para o topo da pilha e deleta
git stash pop 
# vai para o stash especificado e deleta
git stash pop stash@{n} passa já deletando
```

E também temos o subcomando drop que deleta e não pega as modificações

```bash
# remove exatamente qual stash você quer remover
git stash drop stash@{n} 
```

Para mais informações

[https://git-scm.com/docs/git-stash](https://git-scm.com/docs/git-stash)
