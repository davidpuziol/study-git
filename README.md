# Study Git

![gitlogo](./pics/git.svg)

Essa documentação trás os pricipais comandos do git. Abranger tudo é muito extenso e muitas vezes não chegamos de fato a usar tudo nem mesmo lembrar.

Um sistema de controle de versões distribuído, ou seja, um software que rastreia os arquivos e controla o que você tem na sua máquina com o que existe no repositório.

## Vantagens do uso

- **Histórico**

  - Alternar entre versões (log)

  - Tranquilidade de saber que tem um backup, um plano B

- **Trabalho em Equipe**

  -Várias pessoas trabalhando em paralelo no mesmo projeto

- **Ramificação**

  - Permite que você criar várias versões a partir de um ponto (branches)

  - Junção das funcionalidades após finalizadas (merges)

- **Rastreabilidade**

  - dentificar em que ponto a mudança foi feita
  
  - Identificar o responsável pela mudança
