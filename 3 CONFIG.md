# Personalizando o git

Para alterar configurações do git usamos o comando `git config`.

[https://git-scm.com/docs/git-config](https://git-scm.com/docs/git-config)

É necessário configurar algumas variáveis que o git usa para rastrear o usuário que fez as mudanças no arquivo e comitou, lembrando que as variáveis para o git são case sensitive. Por padrão é bom usar no nome a primeira letra maiúscula.

A configuração esta associada a um nível porém a sua **configuração do git é o somatório de todos os níveis**, sendo que a configuração do repositório (local)tem preferência à configuração do usuário (global) e esta sob a configuração do sistema (system).

Para o comando git config saber em qual nível irá configurar o escopo.

- **--system** são configurações em nível de sistema, todos os usuários terão a mesma configuração (evite)
  - no linux fica em /etc/gitconfig e só é criado quando a primeira configuração for feita.
  - no windows fica em C:\Documents\Git\etc\gitconfig

- **--global** são configurações a nível de usuário
  - no linux fica em ~/.gitconfig na raiz da home do usuário.
  - no windows fica em C:\Users\<UserName>\.gitconfig

- **--local** são configurações a nível de repositório, ou seja, o diretório precisa ter um .git e estará dentro de desta pasta um config.

```bash
git config --global user.name “Fulano Tall”

git config --global user.email “fulanotal@gmail.com”
```

git config --list --show-scope

Algumas outras configurações podem ser legais, mas não necessária.

```bash
                    #nomedobloc.config
git config --global core.editor vim 
git config --global color.branch auto
git config --global color.diff auto
git config --global color.interactive auto
git config --global color.status auto
```

Tudo pode ser configurado através da cli ou editado direto no arquivo gitconfig correto. A vantagem do uso da cli é evitar erros, então tome cuidado ao editar o arquivo.

para abrir o arquivo diretamente e editar

```bash
# estando no diretório do projeto
git config --edit 
# pro seu usuário
git config --global --edit
# geral
sudo git config --system --edit
```

Exemplo do meu gitconfig ao nível do meu usuário.

```bash
❯ cat ~/.gitconfig
# bloco                   
[user]
        # confis
        name = David Puziol
        email = david@gmail.com
[core]
        editor = vim
[color]
        branch = auto
        diff = auto
        interactive = auto
        status = auto
[alias]
        ec = git config --global -e
        co = checkout
        cob = checkout -b
        c = commit
        cm = commit -m
        cma = commit -a --ammend
        lg = log — all — graph — decorate — oneline — abbrev-commit
        ac = !git add -A && git commit -m
        df = diff
        a = add
        aa = add -A
        tags = tags -l
        branches = branch -a
        up = !git pull --rebase --prune $@ && git submodule update --init --recursive
        undo = reset HEAD~1 --mixed
        wip = !git add -A && git commit -qm \"WIPE SAVEPOINT\" && git reset HEAD~1 --hard
        pullall = "!git branch -r | grep -v \"\\->\" | while read remote; do git branch --track ${remote#origin/} ${remote}; done && git fetch --all && git pull --all"
```

Observe que core.name e core.email estão no mesmo bloco [core]

Alias são atalhos que podemos criar para chamar substituir parte da linha de código para facilitar a vida, mas só faça isso depois que conhecer bem os comandos.

Por exemplo nesse repositório temos um .git

```bash
~/study-git/.git develop                                                                     
❯ cat config
# nome do bloco
[core]
        # config do bloco
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
        editor = vim
[remote "origin"]
        url = git@gitlab.com:davidpuziol/study-git.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "main"]
        remote = origin
        merge = refs/heads/main
```

É possível configurar uma branch, no caso do exemplo abaixo main, como sendo padrão para o pull (baixar)e push (subir).

```bash
git config --local init.defaultBranch main
```
