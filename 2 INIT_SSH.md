# Iniciando um repositório

## Autenticação SSH no Git

Antes de começar a falar do git faça uma conta no [gitlab](http://gitlab.com/) ou no [github](https://github.com/) e vincule uma chave ssh para não ter que ficar digitando usuário e senha o tempo inteiro, pois é chato.

```bash
ssh-keygen -b 2048 -t rsa
```

Esse comando gera duas chaves, uma publica (ir_rsa.pub) e uma privada (ir_rsa) que ficam no seu diretório de usuário na pasta .ssh. Pense que a publica é a fechadura e a privada é a chave. Só você pode ter a chave mas você pode instalar a fechadura em qualquer lugar.

Copie o conteúdo da chave publica para o local abaixo

![sshgit](./pics/gitlssh.png)

Exemplo de url para atenticação padrão

- `https://gitlab.com/davidpuziol/teste.git`

Exemplo de url para autenticação com ssh

- `git@gitlab.com:davidpuziol/teste.git`

## Comando Init

Toda vez que temos uma pasta/ projeto  dentro teremos um arquivo oculto de controle do git chamado **.git**. Uma pasta que não tem esse arquivo não está sendo controlada pelo git. Para isso é necessário o comando dentro da pasta.

Geralmente você pode ir no repositório e crir um projeto e depois clonar ele que o .git já estará vinculado ao repositório. Vou criar o projeto meurepo e clonar usando a ssh e mostrar o .git

![meurepo](./pics/repoexample.png)

```bash
❯ git clone git@gitlab.com:davidpuziol/meurepo.git
Cloning into 'meurepo'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (3/3), done.

❯ cd meurepo 
❯ ls -lha
total 20K
drwxrwxr-x  3 david david 4,0K jun 25 07:22 .
drwxrwxr-x 11 david david 4,0K jun 25 07:05 ..
drwxrwxr-x  8 david david 4,0K jun 25 07:22 .git
-rw-rw-r--  1 david david 6,1K jun 25 07:22 README.md
❯ cat .git/config 
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
# observe que o origin já veio configurado e fetch também
[remote "origin"]

# veja que a url que utilizamos aqui não começa com http, logo estará usando a chave ssh para autenticação
        url = git@gitlab.com:davidpuziol/meurepo.git
        fetch = +refs/heads/*:refs/remotes/origin/*
# também já veio definida uma branch chamda main
[branch "main"]
        remote = origin
        merge = refs/heads/main
```

mas vamos fazer isso de forma manual, criando um projeto na própria máquina e depois criando um repo e vinculando um projeto já existente para um repo novo.

O comando **git init** cria por padrão a branch (ramificação) chamada master, mas hoje em dia usa-se main, logo podemos mudar isso inicialmente passando o nome da branch

```bash
❯ mkdir teste
❯ cd teste
# já estou criando definindo o nome da branch para main
❯ git init -b main
Repositório vazio Git inicializado em  /home/david/projects/pessoais/teste/.git/
❯ cat .git/config 
[core]
        repositoryformatversion = 0
        filemode = true
        bare = false
        logallrefupdates = true
```

Vamos agora imaginar que já fizemos algo, só para entender melhor. Não precisa entender todos os comandos agora.

```bash
# criamos 3 files na branch padrão (main)
touch arquivo1 arquivo2 arquivo3
git add .
git commit -m "first init"
# criamos uma nova branch develop baseada na padrão que estamos no momento
git branch develop
# mudamos para a branch develop
git checkout develop
# criamos mais um arquivo e adicionamos
touch arquivo4
git add .
git commit -m "add arquivo4"
```

agora temos 2 branchs, uma main com 3 arquivos e uma develop com 4 arquivos.

vamos vincular isso a um repositório. crie um repo.

agora vamos adicionar as configurações

```bash
# adicionando a origem do repo
❯ git remote add origin git@gitlab.com:davidpuziol/teste.git
# Se você criou a master no lugar da main e no seu repo ele tem a main, você precisa mover a master para main
❯ git branch -M main

# Se você criou um repo com um arquivo README.md por exemplo você precisa traze-lo fazer um rebase da sua branch main local com a branch main do repo. Sua branch main não tem os arquivos que já tem na branch main do repositório.
❯ git pull origin main --rebase
From gitlab.com:davidpuziol/teste
 * branch            main       -> FETCH_HEAD
Successfully rebased and updated refs/heads/main.
# Agora vamos subir a main local na main do repo
❯ git push origin main
Enumerating objects: 6, done.
Counting objects: 100% (6/6), done.
Delta compression using up to 12 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (5/5), 483 bytes | 483.00 KiB/s, done.
Total 5 (delta 1), reused 0 (delta 0), pack-reused 0
To gitlab.com:davidpuziol/teste.git
   54fdd8a..f2d4649  main -> main
```

Lembrando que você subiu só main local, ainda falta a develop

```bash
# para passar para a branch develop e subindo também para o repositório
git checkout develop
git push origin develop
```

Conferindo...

![repomanual](./pics/repomanual.png)

Toda vez que você dá um push e um pull ele não sabe a que branch você está se referindo, por isso usa-se o origin. No config é possíel definir a branch default.
