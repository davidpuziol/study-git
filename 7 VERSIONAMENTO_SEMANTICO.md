# Versionamento Semântico

O versionamento semântico tem 3 numerais.

![version](./pics/versionamentosemantico.png)

Regra:
Toda vez que o Major sobe, Minor e Patch são zerados.

Toda vez que Minor sobe, patch é zerado.

## Sobre o Minor

Cada Minor que sobe é uma funcionalidade extra que o software recebeu.

Toda funcionalidade extra geram seus erros e bugs que devem ser consertados e também versionados usando o Patch.

## Sobre o Patch

O Patch são o número de bugs que aquela funcionalidade extra gerou e foi sendo consertado.

Se acabou os bugs e vai passar para uma próxima funcionalidade aumenta o minor e zera o patch

Já estou na versão 1.13.4 e encontrei bug da versão 1.12.X não tem problema, corrige e passa para a 1.13.5 simples assim.

## Sobre o Major

o Major é mudado sempre que uma alteração grande ou quebra de contrato com a API acontece.

Para o major, se o software não tem uma versão estável ele tem que começar com 0 sendo que o início de tudo é 0.1.0
