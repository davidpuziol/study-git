# Gitflow

O Gitflow é uma extensão do Git. É um workflow de fluxo de desenvolvimento de software para Git.

Qualquer equipe pode ter o seu workflow específico desde que todos o conheçam. O Gitflow é workflow é uma proposta já consolidada em equipes grandes que funciona bem, então não vamos reinventar a roda.

Seria uma metodologia de ramificação para oferecer uma estrutura robusta e gerenciamento de projetos maiores.

**Pode-se aplicar um gitflow sem a extensão nenhuma desde que conheça a sequência**, mas as funcionalidades da extensão facilitam o trabalho.

É atribuída funções específicas para diferentes branches  e define quando elas devem interagir. Além das ramificações de recursos, ele utiliza ramificações individuais para preparar, manter e registrar lançamentos.

O Gitflow é uma ideia de fluxo de trabalho Git. Isto significa que ele dita que tipos de ramificações configurar e como fazer a mesclagem.

## Instalação

Em windows instale o git normalmente que o flow já está incluso.

```bash
# Linux ubuntu
sudo apt-get install git-flow
# Mac
brew install git-flow
```

## Auto complete

Para usar com o bash somente precisa

```bash
sudo apt-get install git bash-completion -y
```

Para zsh e oh my zsh

```bash
wget -O https://sourceforge.net/p/zsh/code/ci/master/tree/Completion/Unix/Command/_git\?format\=raw > _gitflow.zsh
mv _gitflow.zsh ~/.oh-my-zsh/completions/_gitflow.zsh
source ~/.zshrc
```

## Comando flow Init

git flow init

É uma extensão do comando padrão para inicializar um repositório local git init. Não altera nada no repositório, a não ser criar as ramificações para você.

![gitflow](./pics/gitflow.png)

Branch principal (main) e de desenvolvimento (develop)
Em vez da única branch main, este fluxo de trabalho usa duas branches para registrar o histórico do projeto. A branch main armazena o histórico do lançamento oficial, e a branch develop serve como uma ramificação de integração para recursos. Também é conveniente marcar todas as confirmações na branch main com um número de versão usando uma tag.

A primeira coisa é criar uma branch develop com os comandos.

```bash
# cria a branch localmente
git branch develop 
# sobe a branch
git push -u origin develop o
```

Se um projeto já foi criado com uma branch develop no repositório não é necessário o comando acima.

Uma boa prática em todo projeto é criar uma branch develop em todos os projetos.

Esta branch develop vai conter o histórico completo do projeto, enquanto que a branch main, a principal, vai conter uma versão abreviada. Outros desenvolvedores agora vão precisar clonar o repositório e criar uma ramificação de rastreamento para a de develop.

Ao utilizar a biblioteca de extensão do git-flow, executar git flow init no repositório existente vai criar uma ramificação de desenvolvimento:

```bash
git flow init
Initialized empty Git repository in ~/project/.git/

No branches exist yet. Base branches must be created now.

Branch name for production releases: [master] main

Branch name for "next release" development: [develop]

How to name your supporting branch prefixes?

Feature branches? [feature/]

Release branches? [release/]

Hotfix branches? [hotfix/]

Support branches? [support/]

Version tag prefix? []
```

Veja a saída

```bash
git branch

develop

 main
```

### Branches de Features

Cada nova feature deve residir na própria ramificação, a qual pode ser enviada por push para o repositório do projeto. No entanto, em vez de serem ramificações da branch main (principal), as ramificações das features usam a branch develop (desenvolvimento) mais recente como ramificação pai. Quando um recurso é concluído, ele é mesclado de volta na branch develop. Os recursos não devem nunca interagir com a branch main.

para iniciar nova feature seria

Sem as extensões do git-flow:

```bash
git checkout develop

git checkout -b feature_branch
```

Ao usar a extensão do git-flow:

```bash
git flow feature start feature_branch
```

E para finalizar
Sem as extensões do git-flow:

```bash
git checkout develop
git merge feature_branch
```

Ao usar a extensão do git-flow:

```bash
git flow feature finish feature_branch
```

### Branch de Release

Quando a branch develop adquiriu recursos o bastante para uma release ou a data de lançamento esta se aproximando, você bifurca uma ramificação de lançamento a partir da de desenvolvimento. Nesse ponto nenhum novo recurso deve ser adicionado, somente correção de bugs e testes, geração de documentação e outras tarefas. Quando estiver pronta essa release, a branch de release é mesclada com a branch main e marcada com um número de versão. Além disso, ela deve ser mesclada de volta com a branch develop, a qual pode ter progredido desde que o lançamento foi iniciado.

Sem as extensões do git-flow:

```bash
git checkout develop

git checkout -b release/0.1.0
```

Ao utilizar extensões do git-flow:

```bash
git flow release start 0.1.0
```

Depois de pronto, o release vai ser mesclado na branch main e na develop e, então, a branch de release vai ser excluída. O processo de mesclar de volta com branch develop é importante porque atualizações importantes podem ter sido adicionadas à ramificação de release e elas devem ser acessíveis as novas features. É o ponto ideal para um pull.

Para finalizar a branch de release

Sem as extensões do git-flow:

```bash
git checkout main

git merge release/0.1.0
```

Ou, com a extensão do git-flow:

```bash
git flow release finish '0.1.0'
```

### Branches de Hotfix

As branches de manutenção ou de “hotfix” são usadas para corrigir com rapidez lançamentos de produção. As ramificações de hotfix se parecem com ramificações de lançamento e de recurso, com a diferença de serem baseadas a partir da branch main em vez da de develop. Esta é a única ramificação que deve ser bifurcada com objetividade a partir da branch main. Assim que a correção é concluída, ela deve ser mesclada tanto na branch main quanto na de develop (ou na ramificação de lançamento atual) e a branch main deve ser marcada com um número de versão atualizado.

Sem as extensões do git-flow:

```bash
git checkout master
git checkout -b hotfix_branch
```

Ao utilizar extensões do git-flow:

```bash
git flow hotfix start hotfix_branch
```

Assim como na finalização da ramificação de release, a ramificação de hotfix é mesclada tanto na branch main quanto na de develop.

Sem as extensões do git-flow

```bash
git checkout master
git merge hotfix_branch
git checkout develop
git merge hotfix_branch
git branch -D hotfix_branch
```

Com git-flow

```bash
git flow hotfix finish hotfix_branch
```

git flow é só um fluxo de trabalho, mas a extensão facilita a vida!
